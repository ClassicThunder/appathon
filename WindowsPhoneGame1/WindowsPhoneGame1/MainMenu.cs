using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace WindowsPhoneGame1 {
   
    public class MainMenu : Component {

        public int LastScore { get; set; }

        private Vector2 NewGameLocation { get; set; }
        private Rectangle NewGame { get; set; }

        private Vector2 QuitLocation { get; set; }
        private Rectangle Quit { get; set; }

        private SpriteBatch SpriteBatch { get; set; }
        private SpriteFont SpriteFont { get; set; }        
        private GraphicsDevice GraphicsDevice { get; set; }

        private Texture2D Title { get; set; }        

        public MainMenu(Game1 game) : base(game) { }

        public override void Init() {

            var size = SpriteFont.MeasureString("New Game");
            var locationX = (320 / 2) - (int)(size.X / 2);
            var locationY = 100 + (400 / 2) - SpriteFont.LineSpacing - (int)(size.Y / 2);
            NewGame = new Rectangle(locationX, locationY, (int)size.X, (int)size.Y);
            NewGameLocation = new Vector2(locationX, locationY);

            size = SpriteFont.MeasureString("Quit");
            locationX = (320 / 2) - (int)(size.X / 2);
            locationY = 100 + (400 / 2) - (int)(size.Y / 2);
            Quit = new Rectangle(locationX, locationY, (int)size.X, (int)size.Y);
            QuitLocation = new Vector2(locationX, locationY);

            Song classicTheme = Game.Content.Load<Song>("Original Tetris Theme - Extended 10min");
            MediaPlayer.Play(classicTheme);
        }        

        public override void LoadContent(GraphicsDevice device, ContentManager content) {

            GraphicsDevice = device;
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            SpriteFont = content.Load<SpriteFont>("SpriteFont2");
            Title = content.Load<Texture2D>("title");            
        }

        public override void Update() {

            var current = TouchPanel.GetState();
            foreach (var tl in current) {
                var location = new Point((int)tl.Position.X, (int)tl.Position.Y);
                if (tl.State == TouchLocationState.Pressed) {
                    if (NewGame.Contains(location)) {
                        Game.Component = Game.ComponentArray[1];
                        Game.Component.Init();
                        MediaPlayer.Stop();
                        return;
                    } 
                    
                    if (Quit.Contains(location)) {
                        Game.Exit();
                    }
                }
            }
        }

        public override void Draw() {

            GraphicsDevice.Clear(Color.CornflowerBlue);

            SpriteBatch.Begin();

            SpriteBatch.Draw(Title, Vector2.Zero, Color.White);

            SpriteBatch.DrawString(SpriteFont, "New Game", 
                new Vector2(NewGameLocation.X + 2, NewGameLocation.Y + 2), Color.LightSlateGray);
            SpriteBatch.DrawString(SpriteFont, "New Game", NewGameLocation, new Color(174, 0, 0));

            SpriteBatch.DrawString(SpriteFont, "Quit",
                new Vector2(QuitLocation.X + 2, QuitLocation.Y + 2), Color.LightSlateGray);
            SpriteBatch.DrawString(SpriteFont, "Quit", QuitLocation, new Color(174, 0, 0));

            var str = "Last Score: " + LastScore;
            var size = SpriteFont.MeasureString(str);
            var locationX = (320 / 2) - (int)(size.X / 2);
            var locationY = 200 + (400 / 2) - SpriteFont.LineSpacing - (int)(size.Y / 2);
            SpriteBatch.DrawString(SpriteFont, str, new Vector2(locationX, locationY), Color.Black);

            SpriteBatch.End();  
        }
    }
}
