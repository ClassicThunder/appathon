using Microsoft.Devices.Sensors;

namespace WindowsPhoneGame1 {
    
    public abstract class Orientation {
        public abstract bool Valid(AccelerometerReadingEventArgs e);
    }    

    public class Up : Orientation {
        public override bool Valid(AccelerometerReadingEventArgs e) {
            if (e.Y < -.6 && e.X < .4 && e.X > -.4) {
                return true;
            }

            return false;
        }
    }

    public class Down : Orientation {
        public override bool Valid(AccelerometerReadingEventArgs e) {
            if (e.Y > .6 && e.X < .4 && e.X > -.4) {
                return true;
            }

            return false;
        }
    }

    public class Left : Orientation {
        public override bool Valid(AccelerometerReadingEventArgs e) {
            if (e.X > .6 && e.Y < .4 && e.Y > -.4) {
                return true;
            }

            return false;
        }
    }

    public class Right : Orientation {
        public override bool Valid(AccelerometerReadingEventArgs e) {
            if (e.X < .6 && e.Y < .4 && e.Y > -.4) {
                return true;
            }

            return false;
        }
    }
}
