using System;
using System.Collections.Generic;
using Microsoft.Devices.Sensors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace WindowsPhoneGame1 {
   
    public class GameLoop : Component {

        private int _tick;

        private event Rotated OnRotate;
        private Orientation _or;
        private Orientation Current {
            get { 
                return _or; 
            } set {
                if (OnRotate != null)
                {
                    OnRotate(_or);
                }
                _or = value;
            } 
        }
        readonly Orientation[] _orientations = new Orientation[] { new Up(), new Down(), new Left(), new Right() };

        private Texture2D Buttons { get; set; }

        private Tetris Tetris { get; set; }
        Vector3 _accelReading;
        Accelerometer _accelSensor;
        readonly GraphicsDeviceManager _graphics;
        SpriteBatch _spriteBatch;
        SpriteFont _font;

        public static bool Accelerate { get; set; }
        public static Texture2D White;


        private static readonly Random Random = new Random();
        public static Texture2D RandomTexture() {
            var holder = Random.Next(Textures.Count);
            return Textures[holder];
        }
        public static List<Texture2D> Textures { get; set; }
        public static List<Song> Songs { get; set; }
        
        public GameLoop(Game1 game) : base(game) {

            _accelReading = new Vector3();            

            _accelSensor = new Accelerometer();
            _accelSensor.Start();
            Current = _orientations[0];
            _accelSensor.ReadingChanged += (sender, e) => {
                _accelReading.X = (float)e.X;
                _accelReading.Y = (float)e.Y;
                _accelReading.Z = (float)e.Z;

                foreach (var orientation in _orientations) {
                    if (orientation.Valid(e)) {
                        Current = orientation;
                    }
                }
            };
        }

        public override void Init() {
            TickSpeed = 30;

            Tetris = new Tetris(Game);
            OnRotate += rotation => Tetris.HandleRotate(rotation);
        }

        private Texture2D GameBoard { get; set; }
        private GraphicsDevice GraphicsDevice { get; set; }
        public override void LoadContent(GraphicsDevice device, ContentManager content)
        {

            GraphicsDevice = device;
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            White = new Texture2D(GraphicsDevice, 1, 1);
            White.SetData(new[] { Color.White });

            _font = content.Load<SpriteFont>("SpriteFont1");

            Textures = new List<Texture2D> {
                content.Load <Texture2D>("blueBlock"),
                content.Load <Texture2D>("greenBlock"),
                content.Load <Texture2D>("orangeBlock"),
                content.Load <Texture2D>("purpleBlock"),
                content.Load <Texture2D>("redBlock"),
                content.Load <Texture2D>("tealBlock"),
                content.Load <Texture2D>("yellowBlock")
            };

            Songs = new List <Song> {
                content.Load <Song>("DEFUNK - Tetris Dubstep"),
                content.Load <Song>("Original Tetris theme (Tetris Soundtrack)"),
                content.Load<Song>("DJ Rush - Tetris Russian Theme (Techno Remix)"),
                content.Load<Song>("Tetris - Amazing Remix"),
                content.Load<Song>("Tetris"),
            };

            GameBoard = content.Load<Texture2D>("gameBoard");

            Buttons = content.Load<Texture2D>("Button");

            MediaPlayer.MediaStateChanged += new EventHandler<EventArgs>(MediaPlayer_MediaStateChanged);
        }
    void MediaPlayer_MediaStateChanged(object sender, EventArgs e) {
        if (MediaPlayer.State != MediaState.Playing) {
            Song nextSong = Songs[1];
                if (Tetris.Level % 2 == 0) {
                        nextSong = Songs[Random.Next(4)];
                }
                if (Tetris.Level % 3 == 0) {
                        nextSong = Songs[2];                  
                }
                if (Tetris.Level % 5 == 0) {
                        nextSong = Songs[3];
                }
                if (Tetris.Level % 7 == 0) {
                        nextSong = Songs[0];
                }
                if (Tetris.Level % 11 == 0) {
                        nextSong = Songs[4];
                }
                if (Tetris.Level == 0) {
                        nextSong = Songs[1];
                }
                if (Tetris.Level == 1) {
                        nextSong = Songs[2];
                }

                        MediaPlayer.Play(nextSong);
                }
            }

        private static int TickSpeed { get; set; }
        public static void IncreaseSpeed() {
            if (TickSpeed > 2) {
                TickSpeed -= 1;
            }
        }

        public override void Update() {

            _tick++;
            if (Accelerate) {
                if (_tick >= 2) { _tick = 0; }
            } else {
                if (_tick >= TickSpeed) { _tick = 0; }
            }

            if (_tick == 0) { Tetris.Update(); }

            Tetris.HandelInput();            
        }

        public override void Draw() {

            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();

            _spriteBatch.Draw(GameBoard, new Rectangle(0, 0, 320, 400), Color.White);

            Tetris.Draw(_spriteBatch);

            _spriteBatch.DrawString(_font, 
                "Score\n" + Tetris.Score + 
                "\nLevel\n" + Tetris.Level + 
                "\nLines\n" + Tetris.Lines, 
                Vector2.Zero, Color.Black);

            _spriteBatch.Draw(Buttons, new Rectangle(0, 160, 50, 50), null, Color.White * 0.4f, 0.0f, Vector2.Zero, SpriteEffects.FlipHorizontally, 0.0f);
            _spriteBatch.Draw(Buttons, new Rectangle(320 - 50, 160, 50, 50), Color.White * 0.4f);

            _spriteBatch.End();  
        }
    }
}
