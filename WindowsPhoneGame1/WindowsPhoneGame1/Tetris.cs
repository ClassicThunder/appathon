using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Rectangle = Microsoft.Xna.Framework.Rectangle;
using System;

namespace WindowsPhoneGame1 {

    public class Tetris {

        private Game1 Game { get; set; }

        private Queue<Shape> Next { get; set; }
        private Shape Current { get; set; }

        private int currentScore = 0;
        public int Score { get { return currentScore; } }

        private int currentLevel = 0;
        public int Level { get { return currentLevel; } }

        private int lineCounter = 0;
        public int Lines { get { return lineCounter; } }
        
        private int _currentIndex;
        private int CurrentIndex {
            get { return _currentIndex; }
            set { _currentIndex = value % _shapes.Length; }
        }

        private readonly Shape[] _shapes = new Shape[] { new SBlock(), new SBlockMirror(), new Line(), new LBlock(), new LBlockMirror(), new WindowBlock(), new TBlock() };
        private readonly static Random _random = new Random();
        public Shape RandomShape() {
            return _shapes[_random.Next(_shapes.Length)];
        }

        readonly Texture2D[,] _tiles = new Texture2D[10, 20];

        public Tetris(Game1 game) {

            Game = game;

            Current = _shapes[CurrentIndex];

            for (var x = 0; x < _shapes.Length; x++) {
                _shapes[x].Texture2D = GameLoop.Textures[x % GameLoop.Textures.Count];
            }

            Next = new Queue <Shape>();
            while (Next.Count < 4) { Next.Enqueue(RandomShape()); }

            for (var x = 0; x < 10; x++) {
                for (var y = 0; y < 20; y++) {
                        _tiles[x, y] = null;
                    }
                }
            }

        public bool CanRotate(Shape shape, Rotate rotate) {

            var tempColor = shape.TilesFromRotation(rotate);
            var tempArea = shape.AreaFromRotation(rotate);

            if (tempArea.Top < 0) { return false; }
            if (tempArea.Bottom > 20) { return false; }
            if (tempArea.Left < 0) { return false; }
            if (tempArea.Right > 10) { return false; }

            for (var x = 0; x < tempArea.Width; x++) {
                for (var y = 0; y < tempArea.Height; y++) {

                    if (tempColor[x, y] == null) {
                        continue;
                    }

                    var locationX = tempArea.X + x;
                    var locationY = tempArea.Y + y;

                    if (_tiles[locationX, locationY] != null) {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool CanMove(Shape shape, Point offset) {

            if (shape.Area.Top + offset.Y < 0) { return false; }
            if (shape.Area.Bottom + offset.Y > 20) { return false; }
            if (shape.Area.Left + offset.X < 0) { return false; }
            if (shape.Area.Right + offset.X > 10) { return false; }

            for (var x = 0; x < shape.Area.Width; x++) {
                for (var y = 0; y < shape.Area.Height; y++) {

                    if (shape.Tiles[x, y] == null) { continue; }

                    var locationX = shape.Area.X + x + offset.X;
                    var locationY = shape.Area.Y + y + offset.Y;

                    if (_tiles[locationX, locationY] != null) {
                        return false;
                    }
                }
            }
            return true;
        }

        public void HandleRotate(Orientation rotation) {

            Rotate temp;

            if (rotation is Up) { temp = Rotate.Up; } 
            else if (rotation is Down) { temp = Rotate.Down; } 
            else if (rotation is Left) { temp = Rotate.Left; } 
            else if (rotation is Right) { temp = Rotate.Right; } 
            else { return; }

            if (CanRotate(Current, temp)) {
                Current.Rotation = temp;
            }
        }

        public void HandelInput() {
            var current = TouchPanel.GetState();
            foreach (var tl in current) {
                if (tl.State == TouchLocationState.Pressed) {
                    if (tl.Position.Y > 350) {
                        GameLoop.Accelerate = true;
                    } else if (tl.Position.X < 106) {
                        var shift = new Point(-1, 0);
                        if (CanMove(Current, shift)) {
                            Current.Shift(shift);
                        }
                    } else {
                        var shift = new Point(1, 0);
                        if (CanMove(Current, new Point(1, 0))) {
                            Current.Shift(shift);
                            }
                        }
                    } else if (tl.State == TouchLocationState.Released) {
                        GameLoop.Accelerate = false;
                    }
                }
            }

        public void Update() {
            if (Current == null) {
                return;
            }

            var shift = new Point(0, 1);
            if (CanMove(Current, shift)) {
                Current.Shift(shift);
            } else {
                for (var x = 0; x < Current.Area.Width; x++) {
                    for (var y = 0; y < Current.Area.Height; y++) {
                        if (Current.Tiles[x, y] != null) {
                            _tiles[Current.Area.X + x, Current.Area.Y + y] = Current.Texture2D;
                        }
                    }
                }

                areLinesMade();

                CurrentIndex++;
                Current = Next.Dequeue();
                Next.Enqueue(RandomShape());
                Current.Reset();
                if (!CanMove(Current, Point.Zero)) {
                    Game.Component = Game.ComponentArray[0];
                    ((MainMenu) Game.Component).LastScore = currentScore;
                    return;
                }
            }
        }

        public void areLinesMade() {
            int linesCleared = 0;
            for (var y = 0; y < 20; y++) {
                bool toClear = true;
                for (var x = 0; x < 10; x++) {
                    if (_tiles[x, y] == null) {
                        toClear = false;
                        break;
                        }
                    }
                if (toClear) {
                    linesCleared++;
                    clearRow(y);
                    for (var i = 0; i < 10; i++) {
                        Array.Copy(_tiles, 20 * i, _tiles, 20 * i + 1, y);
                    }
                    //y++;
                }
            }

            if (linesCleared == 1) {
                currentScore += 40 * (currentLevel + 1);
            } else if (linesCleared == 2) {
                currentScore += 100 * (currentLevel + 1);
            } else if (linesCleared == 3) {
                currentScore += 300 * (currentLevel + 1);
            } else if (linesCleared > 3) {
                currentScore += 1200 * (currentLevel + 1);
            }

            lineCounter += linesCleared;
            if (lineCounter >= 10) {
                currentLevel++;
                lineCounter = lineCounter - 10;
                GameLoop.IncreaseSpeed();
            }
        }
        

        public void clearRow(int index) {
            for (var x = 0; x < 10; x++) {
                _tiles[x, index] = null;
            }
        }

        public void Draw(SpriteBatch batch) {

            //batch.Draw(GameLoop.White, new Rectangle(60, 0, 10 * 20, 20 * 20), Color.Green);            

            for (var x = 0; x < 10; x++) {
                for (var y = 0; y < 20; y++) {
                    if (_tiles[x, y] != null) {
                        batch.Draw(_tiles[x, y], new Rectangle(60 + (x*20), y*20, 20, 20), Color.White);
                    }
                }
            }

            for (var x = 0; x < Current.Area.Width; x++) {
                for (var y = 0; y < Current.Area.Height; y++) {
                    if (Current.Tiles[x, y] != null) {
                        batch.Draw(
                            Current.Texture2D, 
                            new Rectangle(
                                60 + ((x + Current.Area.X) * 20), 
                                (y + Current.Area.Y) * 20, 
                                20, 
                                20), 
                            Color.White);
                    }
                }
            }

            var count = 0;
            foreach (var shape in Next) {                
                var area = shape.AreaFromRotation(Rotate.Up);
                var tiles = shape.TilesFromRotation(Rotate.Up);
                for (var x = 0; x < area.Width; x++) {
                    for (var y = 0; y < area.Height; y++) {
                        if (tiles[x, y] != null) {
                            batch.Draw(shape.Texture2D, new Rectangle(320 - 60 + (20 * x), 20 + 100 * count + (20 * y), 20, 20), Color.White);
                        }
                    }
                }
                count++;
            }
        }
    }
}
