using System;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework;

namespace WindowsPhoneGame1 {

    delegate void Rotated(Orientation rotation);

    public enum Rotate { Up, Down, Left, Right }

    public class Game1 : Game {

        readonly GraphicsDeviceManager _graphics;

        public Component Component { get; set; }
        public Component[] ComponentArray { get; set; }

        public Game1() {
    
            Content.RootDirectory = "Content";

            _graphics = new GraphicsDeviceManager(this) {
                PreferredBackBufferWidth = 320,
                PreferredBackBufferHeight = 400
             };
            _graphics.ApplyChanges();

            PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;

            TargetElapsedTime = TimeSpan.FromSeconds(1.0f / 60.0f);

            ComponentArray = new Component[] { new MainMenu(this), new GameLoop(this) };
            Component = ComponentArray[0];
        }

        protected override void LoadContent() {
            foreach (var comp in ComponentArray) {
                comp.LoadContent(GraphicsDevice, Content);
                comp.Init();
            }
            
        }

        protected override void UnloadContent() {

        }

        protected override void Update(GameTime gameTime) {           
            Component.Update();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            
            GraphicsDevice.Clear(Color.CornflowerBlue);

            Component.Draw();

            base.Draw(gameTime);
        }
    }
}
