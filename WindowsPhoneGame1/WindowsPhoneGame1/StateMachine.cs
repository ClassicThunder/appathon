using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsPhoneGame1 {
    
    public abstract class Component {

        protected Game1 Game { get; set; }

        protected Component(Game1 game) { Game = game; }

        public abstract void LoadContent(GraphicsDevice device, ContentManager content);

        public abstract void Init();

        public abstract void Update();

        public abstract void Draw();
    }
}
