using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

namespace WindowsPhoneGame1 {
  
    public abstract class Shape {

        public Texture2D Texture2D { get; set; }
        public Rotate Rotation { get; set; }

        public Texture2D[,] Tiles {
            get { return TilesFromRotation(Rotation); }
            set { TilesUp = value; }
        }

        public Texture2D[,] TilesFromRotation(Rotate rotate) {
            switch (rotate) {
                case Rotate.Up:
                    return TilesUp;
                case Rotate.Down:
                    return TilesDown;
                case Rotate.Left:
                    return TilesLeft;
                case Rotate.Right:
                    return TilesRight;
                default:
                    return TilesUp;
            }
        }

        public Rectangle Area {
            get { return AreaFromRotation(Rotation); }
            set { AreaNormal = value; }
        }

        public Rectangle AreaFromRotation(Rotate rotate) {
            switch (rotate) {
                case Rotate.Up:
                    return AreaNormal;
                case Rotate.Down:
                    return AreaNormal;
                case Rotate.Left:
                    return AreaRotated;
                case Rotate.Right:
                    return AreaRotated;
                default:
                    return AreaNormal;
            }
        }

        public void Shift(Point shift) {
            AreaNormal.X += shift.X;
            AreaNormal.Y += shift.Y;

            AreaRotated.X += shift.X;
            AreaRotated.Y += shift.Y;
        }

        private Rectangle AreaNormal, AreaRotated;
        private Texture2D[,] TilesUp, TilesRight, TilesDown, TilesLeft;

        protected abstract void Build();
        protected Shape() {
            Reset();
        }

        public void Reset() {
            Build();

            AreaRotated = new Rectangle(AreaNormal.X, AreaNormal.Y, AreaNormal.Height, AreaNormal.Width);
            TilesLeft = Flip(TilesUp, AreaNormal.Width, AreaNormal.Height);
            TilesDown = Flip(TilesLeft, AreaNormal.Height, AreaNormal.Width);
            TilesRight = Flip(TilesDown, AreaNormal.Width, AreaNormal.Height);
        }

        private static Texture2D[,] Flip(Texture2D[,] tiles, int width, int height) {

            var holder = new Texture2D[height, width];

            for (var x = 0; x < height; x++) {
                for (var y = 0; y < width; y++) {
                    holder[x, y] = tiles[width - y - 1, x];
                }
            }

            return holder;
        }
    }

    public class TestBlock : Shape {

        protected override void Build() {
            Area = new Rectangle(0, 0, 1, 1);
            Tiles = new Texture2D[1, 1] { { GameLoop.White } };
        }
    }

    public class Line : Shape {

        protected override void Build() {
            Area = new Rectangle(0, 0, 1, 4);
            Tiles = new Texture2D[1, 4] { { GameLoop.White, GameLoop.White, GameLoop.White, GameLoop.White } };
        }
    }

    public class SBlockMirror : Shape {
        protected override void Build() {
            Area = new Rectangle(0, 0, 3, 2);
            Tiles = new Texture2D[3, 2] {
                { null, GameLoop.White },
                { GameLoop.White, GameLoop.White },
                { GameLoop.White, null }
            };
        }
    }

    public class SBlock : Shape {
        protected override void Build() {
            Area = new Rectangle(0, 0, 3, 2);
            Tiles = new Texture2D[3, 2] {
                { GameLoop.White, null },
                { GameLoop.White, GameLoop.White },
                { null, GameLoop.White }
            };
        }
    }

    public class LBlock : Shape {
        protected override void Build() {
            Area = new Rectangle(0, 0, 2, 3);
            Tiles = new Texture2D[2, 3] {
                { null, null, GameLoop.White},
                { GameLoop.White, GameLoop.White, GameLoop.White}
            };
        }
    }

    public class LBlockMirror : Shape {
        protected override void Build() {
            Area = new Rectangle(0, 0, 2, 3);
            Tiles = new Texture2D[2, 3] {
                { GameLoop.White, GameLoop.White, GameLoop.White },
                { null, null, GameLoop.White }
            };
        }
    }

    public class WindowBlock : Shape {
        protected override void Build() {
            Area = new Rectangle(0, 0, 2, 2);
            Tiles = new Texture2D[2, 2] {
                {GameLoop.White, GameLoop.White},
                {GameLoop.White, GameLoop.White}
            };
        }
    }

    public class TBlock : Shape {
        protected override void Build() {
            Area = new Rectangle(0, 0, 3, 2);
            Tiles = new Texture2D[3, 2] {
                {null, GameLoop.White},
                {GameLoop.White,       GameLoop.White},
                {null, GameLoop.White}
            };
        }
    }
}
